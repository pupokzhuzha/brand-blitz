package com.Brand.Blitz.guess.logo.app.start

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.Brand.Blitz.guess.logo.app.R
import com.Brand.Blitz.guess.logo.app.other.MainActivity
import com.Brand.Blitz.guess.logo.app.other.replaceActivity

class Splashing : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashing)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(MainActivity())
        }, 500)
    }
}