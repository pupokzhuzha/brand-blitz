package com.Brand.Blitz.guess.logo.app.other

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.Brand.Blitz.guess.logo.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}