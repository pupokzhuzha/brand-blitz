package com.Brand.Blitz.guess.logo.app.game

import android.annotation.SuppressLint
import android.graphics.pdf.PdfDocument.Page
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.Brand.Blitz.guess.logo.app.R
import com.Brand.Blitz.guess.logo.app.databinding.FragmentGameBinding
import com.Brand.Blitz.guess.logo.app.databinding.FragmentStartBinding
import com.Brand.Blitz.guess.logo.app.other.GameQ
import com.Brand.Blitz.guess.logo.app.other.PageInfoData
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur

class GameFragment : Fragment() {

    private var binding: FragmentGameBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private var points = 0
    private lateinit var model : PageInfoData
    private var textBtn = ""
    private lateinit var mBlurView: BlurView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGameBinding.inflate(layoutInflater, container, false)
        mBinding.apply {
            mBlurView = blur
        }
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = GameQ.gen(page)
        mBinding.apply {
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            toMenu.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            image.setImageResource(model.image)
            firstText.text = model.first
            secondText.text = model.second
            thirdText.text = model.third

            first.setOnClickListener {
                textBtn = firstText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }
            }
            second.setOnClickListener {
                textBtn = secondText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }
            }
            third.setOnClickListener {
                textBtn = thirdText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
                    points++
                }
                else{
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
                }
            }

            next.setOnClickListener {
                page++
                if(page == 11){
                    blurBackground()
                    back.isEnabled = false
                    next.visibility = View.GONE
                    result.text = "$points/10"
                }
                else{
                    enableBtns(true)
                    model = GameQ.gen(page)
                    image.setImageResource(model.image)
                    firstText.text = model.first
                    secondText.text = model.second
                    thirdText.text = model.third

                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGrey)
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGrey)
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGrey)
                }
            }
        }
    }

    private fun enableBtns(enable : Boolean){
        if (enable){
            mBinding.apply {
                first.isEnabled = true
                second.isEnabled = true
                third.isEnabled = true
            }
        }
        else{
            mBinding.apply {
                first.isEnabled = false
                second.isEnabled = false
                third.isEnabled = false
            }
        }
    }

    private fun blurBackground() {
        val radius = 8f
        val decorView = activity?.window?.decorView
        val rootView = decorView?.findViewById<ViewGroup>(android.R.id.content)
        val windowBackground = decorView?.background
        if (rootView != null) {
            mBlurView.setupWith(rootView, RenderScriptBlur(requireContext()))
                .setFrameClearDrawable(windowBackground)
                .setBlurRadius(radius)
        }
        mBlurView.visibility = View.VISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}