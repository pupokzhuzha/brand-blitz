package com.Brand.Blitz.guess.logo.app.other

import com.Brand.Blitz.guess.logo.app.R

object GameQ {

    fun gen(page : Int) : PageInfoData{
        return when(page){
            1 -> PageInfoData(R.drawable.boeing, "AirTran", "Boeing", "IBERIA", "Boeing")
            2 -> PageInfoData(R.drawable.wikipedia, "Wikipedia", "Amazing Life", "Birmingham Library", "Wikipedia")
            3 -> PageInfoData(R.drawable.yamaha, "Fernandes", "Yamaha", "GEtL", "Yamaha")
            4 -> PageInfoData(R.drawable.witcher, "Naruto Sign", "House Stark", "Witcher Medallion", "Witcher Medallion")
            5 -> PageInfoData(R.drawable.dove, "Elizabeth Arden", "Dove", "Lancome Paris", "Dove")
            6 -> PageInfoData(R.drawable.msn, "MSN", "Picasa", "Evernote", "MSN")
            7 -> PageInfoData(R.drawable.mark, "Parselmouth", "Red Skull", "Black Mark", "Black Mark")
            8 -> PageInfoData(R.drawable.versace, "Louis Vuitton", "Fendi", "Versace", "Versace")
            9 -> PageInfoData(R.drawable.telegram, "Messenger", "Telegram", "WhatsApp", "Telegram")
            else -> PageInfoData(R.drawable.burberry, "Burberry", "Fred Perry", "Hermes", "Burberry")
        }
    }
}