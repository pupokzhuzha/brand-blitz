package com.Brand.Blitz.guess.logo.app.other

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.replaceActivity(activity: AppCompatActivity) {
    val intent = Intent(this, activity::class.java)
    startActivity(intent)
    this.finish()
}