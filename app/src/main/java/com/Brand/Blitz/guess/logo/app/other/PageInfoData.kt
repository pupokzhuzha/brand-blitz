package com.Brand.Blitz.guess.logo.app.other

data class PageInfoData(
    val image : Int,
    val first : String,
    val second : String,
    val third : String,
    val rightAnswer : String
)
